# Makefile
#
# Makefile for SMDK24XX dltool
#
# (c) 2004,2006 Ben Dooks, Ben Dooks <ben-smdk2440@fluff.org>
#

APP		= smdk-usbdl

# Build tools
CC		= $(CROSS_COMPILE)gcc
LD		= $(CROSS_COMPILE)gcc

# Generic build flags
CPPFLAGS	+= -Isrc -MD
CFLAGS		+= -Wall -Wundef -Wpointer-arith -Wcast-qual
CFLAGS		+= -Wextra -Wshadow -Wimplicit-function-declaration
CFLAGS		+= -Wredundant-decls -Wmissing-prototypes -Wstrict-prototypes
CFLAGS		+= -std=gnu89 -fstack-protector-strong
CFLAGS		+= -O2

# Be silent per default, but 'make V=1' will show all compiler calls.
ifneq ($(V),1)
  Q := @
endif

BUILD ?= debug
ifeq ($(BUILD),release)
  CFLAGS	+= -DNDEBUG
  LDFLAGS	+= -s
else ifeq ($(BUILD),debug)
  CFLAGS	+= -ggdb3
else
  $(error Incorrect BUILD variable)
endif

# Link with libusb
CFLAGS		+= $(shell pkg-config --cflags libusb)
LDLIBS		+= $(shell pkg-config --libs libusb)

SOURCES		= src/dltool.c
OBJS		= $(SOURCES:%.c=%.o)

all: $(APP)

$(APP): $(OBJS)
	@printf "  LD      $(APP)\n"
	$(Q)$(LD) $(LDFLAGS) $^ -o $@ $(LDLIBS)

%.o: %.c
	@printf "  CC      $(*).c\n"
	$(Q)$(CC) $(CFLAGS) $(CPPFLAGS) -o $(*).o -c $(*).c

clean:
	@printf "  CLEAN\n"
	$(Q)-rm -f src/*.o
	$(Q)-rm -f src/*.d
	$(Q)-rm -f $(APP)

.PHONY: all clean

-include $(OBJS:.o=.d)
