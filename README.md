# Intro

This is a USB recovery mode download tool for WinLink's E850-96 board
(Exynos850 SoC). It allows one to recover the board in case it was bricked. The
recovery procedure is done by loading the bootloaders from your PC via USB to
board's RAM and executing those (instead of using bootloaders from board's
eMMC).

# Prepare for usage

Install `libusb`:

```
$ sudo apt install libusb-dev libusb-0.1-4
```

Build the tool:

```
$ make
```

Add current user to the `dialout` group:

```
$ sudo usermod -a -G dialout $USER
```

Log out and log in back in your OS to make above command come into effect.

Add the next lines to your `/etc/udev/rules.d/51-android.rules`:

```
# Samsung Exynos850 fastboot
SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", ATTR{idProduct}=="0002", MODE="0660", GROUP="dialout"

# Samsung Exynos850, USB boot (for smdk-usbdl tool)
SUBSYSTEM=="usb", ATTR{idVendor}=="04e8", ATTR{idProduct}=="1234", MODE="0660", GROUP="dialout"
```

Reload udev rules:

```
$ sudo udevadm control --reload
$ sudo udevadm trigger
```

# Usage

If udev rules weren't added (as described above), items #3 and #4 below will
have to be run with `sudo`.

1. Set switch #1 (of SW1 DIP-switch) to "ON" position
2. Power on and boot the board (by pressing SW2 "POWER" button)
3. Run the recovery tool:

     ```
     $ ./smdk-usbdl
     ```

4. Flash images using `fastboot`. E.g. if the LittleKernel bootloader was
   bricked on eMMC and has to be recovered:

     ```
     $ fastboot flash bootloader images/lk.bin
     ```

   You may also want to flash all other low-level bootloaders if they were
   previously flashed incorrectly:

     ```
     $ fastboot flash bl2        images/bl2.img
     $ fastboot flash el3_mon    images/el3_mon.img
     $ fastboot flash epbl       images/epbl.img
     $ fastboot flash fwbl1      images/fwbl1.img
     $ fastboot flash ldfw       images/ldfw.img
     $ fastboot flash tzfw       images/tzsw.img
     $ fastboot flash bootloader images/lk.bin
     ```

5. Power off the board
6. Set switch #1 (of SW1 DIP-switch) to "OFF" position

# Tool history

* It was originally created by Ben Dooks [1]

    ("smdk-tools-v0.20" tool for S3C2410)
* Then modified for Exynos9510 by Alexander Tarasikov (aka astarasikov) [2]

    (using a different endpoint and sending multiple data chunks at once)
* Then modified by Jan Altensen (aka Stricted) [3]
* E850-96 images were added in this repo by Sam Protsenko

Some other related open-source tools can be also found at [4,5,6,7,8,9,10].


# Tool synopsis

```
smdk-usbdl:

	-f <filename>	change the filename to download
			default is download.dat

	-a <address>	set the download address
			default is 0x30000000

	-s		show any smdk board(s) connected

	-b		select usb bus shown by `-s`

	-d		select usb device shown by `-s`

	-x		enable debugging output
```

# Authors

Ben Dooks <<ben-linux@fluff.org>>
(c) 2004,2006

Alexander Tarasikov <<alexander.tarasikov@gmail.com>>
(c) 2020

Jan Altensen <<info@stricted.net>>
(c) 2023

# References

[1] http://www.fluff.org/ben/smdk/tools/downloads/smdk-tools-v0.20.tar.gz

[2] https://github.com/astarasikov/exynos9610-usb-emergency-recovery

[3] https://github.com/Stricted/exynos9610-usb-emergency-recovery

[4] https://gist.github.com/suapapa/1244403/73d95a53361f00784d16ee5fda220734d22ddb65

[5] http://www.fluff.org/ben/smdk/tools/downloads/

[6] https://gitweb.gentoo.org/repo/gentoo.git/tree/dev-embedded/smdk-dltool/smdk-dltool-0.20-r4.ebuild

[7] https://forum.xda-developers.com/showpost.php?p=18485215&postcount=146

[8] https://github.com/coreos/dev-util/blob/master/host/lib/write_firmware.py

[9] https://android.googlesource.com/device/casio/koi-uboot/+/marshmallow-mr1-wear-release/arch/arm/include/asm/arch-exynos/smc.h

[10] https://github.com/hardkernel/u-boot/blob/odroidxu3-v2012.07/board/samsung/smdk4x12/smc.c

(see SMC_CMD_LOAD_UBOOT)
